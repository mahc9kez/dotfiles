#!/bin/bash
LAST_ENTRY_LINK=""
LAST_UPDATE_TIME=""

ATOM=$(curl -s "https://www.theverge.com/rss/front-page/index.xml")
TG_BOT_API=""
TG_CHAT_ID=""

function atom2tg() {
	if [ -z "$LAST_ENTRY_LINK" ]; then
		echo "$ATOM" | xml sel -N atom="http://www.w3.org/2005/Atom" -t -m "/atom:feed/atom:entry/atom:link" -v @href -n | tac \
		| xargs -I{} curl -o /dev/null -s -F chat_id="$TG_CHAT_ID" -F text={} -F disable_notification=false "https://api.telegram.org/bot${TG_BOT_API}/sendMessage"
	else
		UNREAD=$(echo "$ATOM" | xml sel -N atom="http://www.w3.org/2005/Atom" -t -m "/atom:feed/atom:entry/atom:link" -v @href -n | sed '\|'"${LAST_ENTRY_LINK}"'|,$d' | tac)
		if [ $(echo "$UNREAD" | wc -l) -eq 1 ]; then exit 0; fi
		echo "$UNREAD" | xargs -I{} curl -o /dev/null -s -F chat_id="$TG_CHAT_ID" -F text={} -F disable_notification=false "https://api.telegram.org/bot${TG_BOT_API}/sendMessage"
	fi
}

function update-info() {
	LAST_ENTRY_LINK=$(echo "$ATOM" | xml sel -N atom="http://www.w3.org/2005/Atom" -t -m "/atom:feed/atom:entry/atom:link" -v @href -n | head -n1)
	LAST_UPDATE_TIME=$(date +%Y-%m-%dT%H:%M:%SZ)
	gsed -i '\|^LAST_ENTRY_LINK=|s|.*|LAST_ENTRY_LINK='"\"$LAST_ENTRY_LINK\""'|' $0
	gsed -i '\|^LAST_UPDATE_TIME=|s|.*|LAST_UPDATE_TIME='"\"$LAST_UPDATE_TIME\""'|' $0
}

atom2tg
update-info
