#!/bin/bash

# https://store.line.me/
# https://greasyfork.org/en/scripts/21738-line-sticker-downloader/code

rm *_key*.png productInfo.meta meta.json
PACK=${PWD##*/}
OUTPUT="../output/${PACK}_telegram-stickers"
mkdir -p "$OUTPUT"
#for sticker in ^(tab_*).png; do echo $sticker; done # zsh
shopt -s extglob nullglob
#for sticker in !(tab_*).png; do convert $sticker -background none -gravity SouthWest -extent 512x "$OUTPUT"/$sticker; done
#for sticker in !(tab_*).png; do convert $sticker -background none -gravity Center -extent 512x "$OUTPUT"/$sticker; done
for sticker in !(tab_*).png; do convert $sticker -resize x512 "$OUTPUT"/$sticker; done
convert tab_on@2x.png -background none -gravity Center -extent 100x100 "$OUTPUT"/tab_on@2x.png
