# Dotfiles for macOS

## App Store

* [Readmoo](https://apps.apple.com/us/app/readmoo-%E7%9C%8B%E6%9B%B8/id1051660169)
* [Bandwidth+](https://apps.apple.com/us/app/bandwidth/id490461369)
* [Telegram](https://apps.apple.com/us/app/telegram/id747648890)

## Zsh on iTerm2

```
# grml-zsh-config  
curl -o ~/.zshrc -L --max-redirs 1 'https://git.grml.org/f/grml-etc-core/etc/zsh/zshrc'

# Powerlevel10k  
git clone https://github.com/romkatv/powerlevel10k .local/src/github/powerlevel10k

# https://www.iterm2.com/documentation-images.html
curl -o ~/.local/bin/imgcat https://www.iterm2.com/utilities/imgcat
chmod u+x ~/.local/bin/imgcat

# https://www.iterm2.com/documentation-shell-integration.html
curl -L https://iterm2.com/shell_integration/zsh -o ~/.iterm2_shell_integration.zsh
#source ~/.iterm2_shell_integration.zsh
```

## Homebrew

```
# homebrew/core
brew install apktool axel brotli coreutils cmatrix colordiff cowsay ffmpeg
flac fonttools fortune geoip gradle htop jq macvim markdown mpc
mpd mpdas mpdscribble mpv ncmpcpp neofetch opencc opus-tools p7zip
pass pass-otp progress proxychains-ng pwgen qrencode rig screen sox
tmux translate-shell tree typespeed webp wget youtube-dl zbar zsh
zsh-autosuggestions zsh-completions zsh-syntax-highlighting

# homebrew/cask-fonts
brew install font-clear-sans font-fira-code font-raleway
font-raleway-dots font-source-han-noto-cjk

# homebrew/cask
brew cask install android-platform-tools background-music chromium
db-browser-for-sqlite docker emacs firefox fontforge gimp hex-fiend
iina iterm2 kawa kid3 libreoffice macdown metaz nitroshare oracle-jdk
programmer-dvorak seaglass shadowsocksx-ng standard-notes subler
transmission-remote-gui vlc
brew cask install android-file-transfer android-studio clover-configurator
pandora proxifier the-unarchiver ukelele
```

## Other software

* [Mousecape](https://github.com/alexzielenski/Mousecape)
* [demoo](https://github.com/baron0668/demoo)
* [MyCloverThemes](https://github.com/badruzeus/MyCloverThemes)

## Scripts

[script](.local/src/script)

## Music

```
# mpd + ncmpcpp + (mpdscribble) + mpdas
mkdir ~/.config/{mpd,ncmpcpp,mpdscribble}
ln -s .config/mpd .mpd
#ln -s .config/mpdscribble .mpdscribble

# mpd
touch ~/.config/mpd/{playlists,database}
cp /usr/local/etc/mpd/mpd.conf ~/.config/mpd/mpd.conf && vim $_
brew services start mpd

# ncmpcpp
vim ~/.config/ncmpcpp/config

## mpdscribble
#cp /usr/local/etc/mpdscribble.conf ~/.config/mpdscribble/mpdscribble.conf && vim $_
#brew services start mpdscribble

# mpdas
cp /usr/local/etc/mpdasrc.example ~/.config/mpdasrc && vim $_
vim ~/Library/LaunchAgents/com.hrkfdn.mpdas.plist
launchctl load ~/Library/LaunchAgents/com.hrkfdn.mpdas.plist
launchctl start com.hrkfdn.mpdas
```
